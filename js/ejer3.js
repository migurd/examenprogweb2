const fileInput = document.getElementById('fileInput');
const selectedImage = document.querySelector('.selectedImage');

fileInput.addEventListener('change', (e) => {
  const file = e.target.files[0];
  if (file) {
    const imageURL = URL.createObjectURL(file);
    selectedImage.src = imageURL;
    document.querySelector('.archivoSeleccionado').textContent = file.name;
  }
});
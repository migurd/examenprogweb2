const rangoInput = document.getElementById("rango");
const paisInput = document.getElementById("pais");
const nombreMonedaInput = document.getElementById("nombre-moneda");
const valorCambioInput = document.getElementById("valor-cambio");

// Fetch the JSON data from the server
fetch('../img/paises.json')
  .then(response => response.json())
  .then(data => {
    const jsonData = data;

    // console.log(jsonData);
    rangoInput.addEventListener("input", function () {
      const valorSeleccionado = parseInt(this.value, 10);
      if (!isNaN(valorSeleccionado) && valorSeleccionado >= 1 && valorSeleccionado <= jsonData.length) {
        const selectedData = jsonData[valorSeleccionado - 1];
        console.log(selectedData.valor_cambio);
        paisInput.value = selectedData.pais;
        nombreMonedaInput.value = selectedData.moneda;
        valorCambioInput.value = selectedData.valor_cambio;
      } else {
        console.error("Valor no válido:", this.value);
      }
    });
  })
  .catch(error => console.error('Error fetching JSON:', error));
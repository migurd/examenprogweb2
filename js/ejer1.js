const tablaMultiplicar = document.querySelector('.tabla');
const btnMostrarTabla = document.querySelector('.btnMostrarTabla');
const comboBox = document.querySelector('#numberCombo');

// I DID IT RAAAAAAAAAAAAGH

btnMostrarTabla.addEventListener('click', () => {
  tablaMultiplicar.innerHTML = '';
  let newRow = document.createElement('div');
  let cbValue = comboBox.value;

  for(let i = 1; i < 11; i++) {
    let result = cbValue*i; 
    if((result) < 10)
    {
      newRow.innerHTML +=
      `
        <div class="row">
          <span class="ele e${cbValue}"></span>
          <span class="ele ex"></span>
          <span class="ele e${i}"></span>
          <span class="ele eigual"></span>
          <span class="ele e${result}"></span>
        </div>
      `;
    }
    else if((result) < 100)
    {
      newRow.innerHTML +=
      `
        <div class="row">
          <span class="ele e${cbValue}"></span>
          <span class="ele ex"></span>
          <span class="ele e${i}"></span>
          <span class="ele eigual"></span>
          <span class="ele e${getOneCharacterFromNumber(result, 0)}"></span>
          <span class="ele e${getOneCharacterFromNumber(result, 1)}"></span>
        </div>
      `;
    }
    else
    {
      newRow.innerHTML +=
      `
        <div class="row">
          <span class="ele e${cbValue}"></span>
          <span class="ele ex"></span>
          <span class="ele e${i}"></span>
          <span class="ele eigual"></span>
          <span class="ele e${getOneCharacterFromNumber(result, 0)}"></span>
          <span class="ele e${getOneCharacterFromNumber(result, 1)}"></span>
          <span class="ele e${getOneCharacterFromNumber(result, 2)}"></span>
        </div>
      `;
    }
  }

  tablaMultiplicar.appendChild(newRow);
  
});

function getOneCharacterFromNumber(number, p) {
  let numberStr = number.toString();

  if (p >= 0 && p < numberStr.length) {
      let character = numberStr[p];
      return character;
  }
}